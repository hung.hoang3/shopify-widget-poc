const url = 'https://80686740047f.ngrok.io';

const replace = require('replace-in-file');
const indexOptions = {
  files: 'public/angular/dist/angular/index.html',
  from: [
    /href="styles/g,
    /src="/g,
  ],
  to: [
    `href="${url}/angular/dist/angular/styles`,
    `src="${url}/angular/dist/angular/`,
  ],
};
replace(indexOptions)
  .then(results => {
    console.log('Replacement results:', results);
  })
  .catch(error => {
    console.error('Error occurred:', error);
  });


// const widgetOptions = {
//   files: 'public/widget.js',
//   from: new RegExp('let authUrl.+'),
//   to: `let authUrl = '${url}/angular/dist/angular/index.html';`,
// };
// replace(widgetOptions)
//   .then(results => {
//     console.log('Replacement results:', results);
//   })
//   .catch(error => {
//     console.error('Error occurred:', error);
//   });


const envOptions = {
  files: '.env',
  from: new RegExp('WIDGET_URL.+'),
  to: `WIDGET_URL=${url}/widget.js`,
};
replace(envOptions)
  .then(results => {
    console.log('Replacement results:', results);
  })
  .catch(error => {
    console.error('Error occurred:', error);
  });
