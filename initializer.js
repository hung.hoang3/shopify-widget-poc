const createScriptTag = async (ctx, accessToken, shop, url) => {
    let bodyObj = {
        script_tag: {
            event: 'onload',
            src: url
          }
    }
    const query = JSON.stringify(bodyObj);
  
    await fetch(`https://${shop}/admin/api/2020-07/script_tags.json`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        "X-Shopify-Access-Token": accessToken,
      },
      body: query
    })
    
    return ctx.redirect('/');
  };
  
  module.exports = createScriptTag;
  