This project contains 3 parts: FE, BE and Shopify application config.
This project is built for POC, so you can find some hard-code data inside,
for make sure anyone who can see this note not only pull but also run it normally

## Overall

In the project directory, you can run:

### `run express`

Runs the backend server, which has built with ExpressJS and NodeJS.<br />
It run on [http://localhost:4000](http://localhost:4000).

Inside folder `/server`, you can find the dummy data for `storefrontToken`
If it is expired, use `Postman` and make a POST request at `https://hung-test-shop.myshopify.com/admin/api/2020-10/storefront_access_tokens.json`
and add to the header: `X-Shopify-Access-Token`: (value of `adminToken`) to get the new one

For the future, a database will be used to storage real token

## Frontend

It's using Angular10 as a core framework

Use `yarn install` to get the latest version of all related libraries

Run `ng serve --port 2000` to host the application at [http://localhost:2000](http://localhost:2000).
