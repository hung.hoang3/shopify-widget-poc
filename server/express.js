const express = require("express");
const fetch = require("node-fetch");
const bodyParser = require("body-parser");

const router = express.Router();
const app = express();
app.use("/", router);

// Static data for test
const adminURL = "https://hung-test-shop.myshopify.com/admin/api/2020-10/graphql.json";
const adminToken = "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";
const storefrontURL = "https://hung-test-shop.myshopify.com/api/2020-10/graphql.json";
const storefrontToken = "8a6ad5f761170827d85e42a1922ae2ef";

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "http://localhost:2000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get("/test1", (req, res) => {
  fetch(storefrontURL, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      "X-Shopify-Storefront-Access-Token": storefrontToken,
      // "X-Shopify-Access-Token": adminToken
    },
    body: JSON.stringify({
      query: `mutation {
        checkoutCompleteWithTokenizedPaymentV3(
        checkoutId: "Z2lkOi8vc2hvcGlmeS9DaGVja291dC81NDU5YTJlYmM0YjIwNTZlMDExNDQ0MWQwNDBhMjJiYj9rZXk9YTBjMjc0ZmZjNmJmMWJkN2IxMzliMzY3YWVlNWUwYTU=",
        payment: {
          paymentAmount: {
            amount: 4000
            currencyCode: VND
          }
          idempotencyKey: "placeholder"
          billingAddress: {
            address1: "Ha noi"
            address2: "Ha Dong"
            city: "Ha noi"
            company: "None"
            country: "VN"
            firstName: "Hung"
            lastName: "Hoang"
            phone: "+84988246261"
            province: "VN"
            zip: "100000"
          },
          paymentData: "placeholder",
          type: GOOGLE_PAY
        }
        ) {
          checkout {
            id
          }
          checkoutUserErrors {
            code
            field
            message
          }
        }
      }`
    })
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      res.send(data);
    })
})

app.get("/test", (req, res) => {
  fetch(storefrontURL, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      // "X-Shopify-Access-Token": adminToken
      "X-Shopify-Storefront-Access-Token": storefrontToken
    },
    body: JSON.stringify({
      query: `mutation {
        checkoutDiscountCodeApplyV2(checkoutId: "MWRjY2VjYzU1ZjViNjFlNzIzNTlkNGVmNmMwMzIwNTA=", discountCode: "DISCOUNT") {
          checkout {
            id
          }
          checkoutUserErrors {
            code
            field
            message
          }
        }
      }`
    })
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      res.send(data);
    })
});

app.post("/create-customer", (req, res) => {
  const data = req.body;

  fetch(storefrontURL, {
    method: "POST",
    headers: {
      "Accept": "application/json",
      "Content-Type": "application/json",
      // "X-Shopify-Access-Token": adminToken
      "X-Shopify-Storefront-Access-Token": storefrontToken
    },
    body: JSON.stringify({
      query: `mutation {
        customerCreate(input: {
          email: "${data.email}",
          password: "${data.password}",
          phone: "${data.phone}",
          firstName: "${data.name}"
        }) {
          userErrors {
            field
            message
          }
          customer {
            id
          }
        }
      }`
    })
  })
    .then(result => {
      return result.json();
    })
    .then(data => {
      res.send(data);
    })
});

const port = 4000;
app.listen(4000, () => console.log("Listening on port " + port + ".... "));