class Advo {
  constructor() {
  }

  initialize() {
    window.loadAdvoView = function (url, setHeader = true, preLoad = false) {
      var $iframeWindow = document.getElementById('advoFillable').contentWindow;
      // $iframeWindow.src = 'https://widget.advo.to/'
      // $iframeWindow.postMessage('https://widget.advo.to/', '*')

      // const div = document.createElement('iframe')
      // document.getElementsByClassName('advo-content')[0].appendChild(div)
      // div.contentWindow.postMessage('', 'https://widget.advo.to/')

      if (window.preLoadPage) {
        advo.fillContent(window.preLoadPage);
        return;
      }

      if (setHeader && !preLoad) {
        $iframeWindow.PT.show_loading();
      }

      if(!setHeader){
        var temp = jQuery.ajaxSettings.headers;
      }

      jQuery.ajax({
        type: 'GET',
        headers: {
          'Access-Control-Allow-Origin': '*',
          'X-Host-Override': 'https://hung-test-shop.myshopify.com',
        },
        url: url,
        success: function (rs) {
          if (preLoad) {
            window.preLoadPage = rs;
            return;
          }

          if (setHeader) {
            $iframeWindow.PT.hide_loading();
          }
          console.log(rs)
          advo.fillContent(rs);
        }
      });

      if(!setHeader) {
        jQuery.ajaxSettings.headers = temp;
      }

    };

    this.authRequire();

    window.advoChangeView = function(slug){
      window.loadAdvoView('https://eber-widget-hung-test-shop.advo.co/new-widget/'+ slug +'?shop=hung-test-shop.myshopify.com&type=shopify', false);
    };

    window.onAdvoIframeLoaded = function () {
      var $iframeContent = document.getElementById('advoFillable').contentWindow;
      //rewrite_url
      jQuery($iframeContent.document).find('a').map(function (i,e,a) {
        let href = jQuery(e).attr('href');
        if((href.indexOf('http://') !== -1 || href.indexOf('https://') !== -1 || href.indexOf('/') === -1)) {
        }
        else{
          jQuery(e).attr('href', window.advoWidgetUrl + '/advo-widget-api'+ href);
        }
      });
      jQuery($iframeContent.document).find("body").on('click', 'a', function (event) {
        if (jQuery(this).hasClass('btnClose')) return;
        if (jQuery(this).attr('target') == "_blank") return;
        if ((jQuery(this).closest('.chatWidget').length > 0 || jQuery(this).closest('.cr-popup').length > 0)) {
          if (jQuery(this).attr('id') !== "download-preview-btn") {
            return;
          }
        } else {
          event.preventDefault();
          if (jQuery(this).attr('href') == "#") {
            jQuery(this).attr('href', 'https://../advo-widget-api');
          }
          let href = jQuery(this).attr('href');
          window.loadAdvoView(href);
        }
      });
      // if (!window.preLoadPage && window.jwtToken != null) {
      //   window.loadAdvoView('https://eber-widget-hung-test-shop.advo.co/advo-widget-api/rewards', true, true);
      // }
    };
  }



  drawWidgetContainer() {
    let container = `<style>
    .advo-align-items-center {
        align-items: center !important;
    }

    .advo-flex-truncate {
        min-width: 0;
        flex: 1;
    }

    .advo-d-flex {
        display: flex !important;
    }

    .advo-ml-2, .advo-mx-2 {
        margin-left: 8px !important;
    }

    .advo-text-truncate {
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;

    }

    /* START ADVO STYLING */

    .advo-container {
        position: fixed;
        width: 0;
        height: 0;
        bottom: 0;
        right: 0;
        z-index: 9999999999;
    }

    .advo-main-frame {
                         height: calc(100% - 120px);
                bottom: calc(100px);
        right: 25px;

        /*Customizable*/

        border-radius: 24px;

        /*===========*/
    }

    .advo-panel-frame {
        background-color: white;
        width: 100px;
        height: 100px;
        position: fixed;
        border: 0;
        outline: 0;
        overflow: hidden;
        z-index: 1;
        box-shadow: 0 0 80px 0 rgba(0, 0, 0, .12);
        -webkit-animation: advoFadeSlideUp .2s ease-in !important;
        animation: advoFadeSlideUp .2s ease-in !important;
    }

    .advo-main-content-wrapper {
        position: absolute;
        width: 100%;
        height: 100%;
        border: 0;
        outline: 0;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
    }

    .advo-content-body {
        margin: 0;
        height: 100%;
        max-width: 100vw;
        /*background-color: #fff;*/
        overflow: hidden;
        transform: translateZ(0);
    }

    .advo-content {
        height: 100%;
    }

    div.advo-btn-open-widget.advo-active{
        background-color: #555555;
    }

    .advo-btn-open-widget{

        /*Customizable*/
        background-color: transparent;
        width: 60px;
        height: 60px;
        border-radius: 25px;

        /*===========*/

        text-align: center;
        cursor: pointer;
        float: right;
        position: fixed;
        bottom: 25px;
        right: 25px;
        z-index: 2147483647;
    }

    .advo-close{
        background: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOBAMAAADtZjDiAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAG1BMVEUAAAD///////////////////////////8AAADr8xjQAAAAB3RSTlMAM7cPx7jIAE21/gAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxIAAAsSAdLdfvwAAABESURBVAjXYxAyYGBgYFZkUHcG0ialDCYlBgzM7slA7MxgUgaUNCkzdgfJMbunlIDUMpiUg7hwGiYOVQfTBzMHZi7UHgCB3RAZ7HszogAAAABJRU5ErkJggg==);
        background-repeat: no-repeat;
        background-position: center center;
        opacity: 0;
        -webkit-transform: rotate(-30deg);
        transform: rotate(-30deg);
        position: absolute;
        top: 0;
        bottom: 0;
        width: 100%;
        transition: opacity .08s linear,-webkit-transform .16s linear;
        transition: transform .16s linear,opacity .08s linear;
        transition: transform .16s linear,opacity .08s linear,-webkit-transform .16s linear;
    }

    .advo-icon{
        background-color: #555555;
        background-image: url(https://advocado-uploads.s3-ap-southeast-1.amazonaws.com/app-icon/ios/120px.png);
        background-position: center center;
        background-size: auto 100%;
        background-repeat: no-repeat;
        opacity: 1;
        -webkit-transform: rotate(0deg) scale(1);
        transform: rotate(0deg) scale(1);
        position: absolute;
        top: 0;
        bottom: 0;
        width: 100%;
        transition: opacity .08s linear,-webkit-transform .16s linear;
        transition: transform .16s linear,opacity .08s linear;
        transition: transform .16s linear,opacity .08s linear,-webkit-transform .16s linear;
        border-radius: 20px;
    }

    .advo-btn-open-widget.advo-active div.advo-icon {
      opacity: 0;
      -webkit-transform: rotate(30deg) scale(0);
      transform: rotate(30deg) scale(0);
    }

    .advo-btn-open-widget.advo-active div.advo-close {
      opacity: 1;
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }


</style>

<style>
    
</style>

<style>
    
</style>

<style>
    @media  screen and (max-width: 420px) {
        .advo-main-frame{
            left: 0 !important;
            right: 0 !important;
            top: 0 !important;
            bottom: 0 !important;
            max-height: unset !important;
            width: 100% !important;
            height: 100% !important;
            z-index: 2147483647 !important;
        }
        .advo-btn-open-widget{
            z-index: 2147483646 !important;
        }
        .advo-main-frame{
            border-radius: 0 !important;
        }
    }
</style>
<div class="advo-container" style="display: none">
    <div class="advo-main-frame advo-panel-frame">
        <div class="advo-main-content-wrapper">
            <div class="advo-content-body">
                <div class="advo-content">
                    <iframe onload="if(typeof window.onAdvoIframeLoaded == 'function') window.onAdvoIframeLoaded()" width="100%" height="100%" class="advo-main-panel-container ui-page-theme-a" id="advoFillable" style="border: none">
                    </iframe>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="advo-btn-open-widget" class="advo-btn-open-widget" data-loaded="false">
    <div class="advo-icon"></div>
    <div class="advo-close"></div>
</div>
`;
    if ($('#previewWidgetContainer').length > 0) {
      $('#previewWidgetContainer').append(container)
    } else {
      $('body').append(container)
    }
  }

  fillContent(content) {
    var doc = document.getElementById('advoFillable').contentWindow.document;
    doc.open();
    doc.write(content);
    doc.close();
    jQuery(doc).find('.advoPage.ui-page').hide().fadeIn();
  }

  authRequire() {
    let authUrl = 'https://widget.advo.to/';
    // let authUrl = ' https://eber-widget-hung-test-shop.advo.co/new-widget/auth-require?shop=hung-test-shop.myshopify.com&type=shopify&login_url=https://hung-test-shop.myshopify.com/account/login&signup=https://hung-test-shop.myshopify.com/account/register';
    window.loadAdvoView(authUrl, false);
  }

  getLoaded(handler) {
    if (window.jQuery) handler();
    else window.setTimeout(() => {
      this.getLoaded(handler);
    }, 20);
  };

  containerEventBinding() {
    // Toggle Button
    $(document).on('click', '#advo-btn-open-widget', function () {
      $(this).toggleClass('advo-active');

      if ($(this).hasClass('advo-active')) {
        if ($(this).data('loaded') == false) {

          $(this).data('loaded', true);
        }
        $('.advo-container').show();
      } else {
        $('.advo-container').hide();
      }
    });
  }
}

let advo = new Advo();

var intervalAdvoJQuery = setInterval(function () {
  if (document.getElementsByTagName('body').length > 0) {
    clearInterval(intervalAdvoJQuery);
    if (!window.jQuery) {
      const script = document.createElement('script');
      script.src = 'https://code.jquery.com/jquery-1.11.3.min.js';
      document.getElementsByTagName('head')[0].appendChild(script);
    }
  } else {
    return;
  }
}, 100);

var getCustomerId = function() {
  try {
    let curr = window.ShopifyAnalytics.meta.page.customerId;
    if (curr !== undefined && curr !== null && curr !== "") {
      return curr;
    }
  } catch(e) { }
  try {
    let curr = window.meta.page.customerId;
    if (curr !== undefined && curr !== null && curr !== "") {
      return curr;
    }
  } catch (e) { }
  try {
    let curr = _st.cid;
    if (curr !== undefined && curr !== null && curr !== "") {
      return curr;
    }
  } catch (e) { }
  try {
    let curr = ShopifyAnalytics.lib.user().traits().uniqToken;
    if (curr !== undefined && curr !== null && curr !== "") {
      return curr;
    }
  } catch (e) { }
  return null;
}

advo.getLoaded(() => {
  window.$ = window.jQuery;

  const customerId = getCustomerId();
  window.parent.customerId = customerId;
  window.parent.isLogged = typeof customerId === 'number';

  window.parent.redirectPage = function(url) {
    window.location.href = window.location.origin + url;
    $('.advo-container').hide();
  };

  window.parent.updateWidgetSize = function(size) {
    const {height, width} = size;
    $('.advo-panel-frame').css('height', height);
    $('.advo-panel-frame').css('width', width);
  };


  advo.drawWidgetContainer();
  advo.initialize();
  advo.containerEventBinding();
});
    
