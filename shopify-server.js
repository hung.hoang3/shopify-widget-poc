require('isomorphic-fetch');
const dotenv = require('dotenv');
const Koa = require('koa');
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const session = require('koa-session');

const path = require('path');
const KoaStatic = require('koa-static');
const cors = require('@koa/cors');
const createScriptTag = require('./initializer');

dotenv.config();
const { default: graphQLProxy } = require('@shopify/koa-shopify-graphql-proxy');
const { ApiVersion } = require('@shopify/koa-shopify-graphql-proxy');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

const { SHOPIFY_API_SECRET_KEY, SHOPIFY_API_KEY, WIDGET_URL } = process.env;

app.prepare().then(() => {
  const server = new Koa();
  server.use(session({ secure: true, sameSite: 'none' }, server));
  server.keys = [SHOPIFY_API_SECRET_KEY];

  server.use(cors());

  server.use(
    createShopifyAuth({
      apiKey: SHOPIFY_API_KEY,
      secret: SHOPIFY_API_SECRET_KEY,
      scopes: [
        'read_products',
        'write_products',
        'unauthenticated_write_customers',
        'unauthenticated_write_checkouts',
        'unauthenticated_read_checkouts',
        'read_customers',
        'write_customers',
        'read_checkouts',
        'write_checkouts',
        'read_orders',
        'write_orders',
        'write_script_tags'
      ],
      accessMode: 'per_user',
      async afterAuth(ctx) {
        const { shop, accessToken } = ctx.session;

        ctx.cookies.set("shopOrigin", shop, {
          httpOnly: false,
          secure: true,
          sameSite: 'none'
        });

        console.log(`Shop ${shop} with access token ${accessToken}`);
        await createScriptTag(ctx, accessToken, shop, WIDGET_URL);
      },
    }),
  );

  const staticDirPath = path.join(__dirname, 'public');
  server.use(KoaStatic(staticDirPath));

  server.use(graphQLProxy({version: ApiVersion.July20}))
  server.use(verifyRequest());
  server.use(async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  });

  server.listen(port, () => {
    console.log(`> Ready on http://localhost:${port}`);
  });
});